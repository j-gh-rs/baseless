package client.network;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import client.network.packet.out.OutPacket;
import client.network.packet.out.TestPacket;

public class NetworkTest implements Runnable {

    
    public static JPanel panel;
    
    public static void main(String[] args) throws InvocationTargetException, InterruptedException {
    	new NetworkTest();
    }
    
    private NetworkTest() throws InvocationTargetException, InterruptedException {
    	SwingUtilities.invokeAndWait(new Runnable() {
    		public void run() {
    	    	JFrame frame = new JFrame();
    	    	
    	    	panel = new JPanel(new GridLayout(7, 4));
    	    	
    	    	frame.setContentPane(panel);
    	    	frame.setSize(500, 700);
    	    	
    	    	frame.setLocationRelativeTo(null);
    	    	frame.setVisible(true);
    		}
    	});
    	
    	Network.start();
    	
    	for (int i = 0; i < 28; i++) {
    		Component c;
    		if (i % 3 == 0) {
    			final int idxFinal = i;
    			c = new PacketButton(new TestPacket() {
    				@Override
    				public void build() {
    					super.writeInt8(idxFinal);
    					super.writeInt8((byte) Math.max(0, (int)(Math.random()*5-2)));
    				}
    			});
			} else {
	    		c = new JLabel();
			}
    		SwingUtilities.invokeAndWait(new Runnable() {
        		public void run() {
	    			panel.add(c);
					panel.revalidate();
					panel.repaint();
        		}
    		});
    	}

		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		executor.scheduleAtFixedRate(this, 0, 3000, TimeUnit.MILLISECONDS);
    }

	@Override
	public void run() {
		int idx = -1;
		while (idx < 28 && !(panel.getComponent(++idx) instanceof JLabel));
		if (idx == 28)
			return;
		System.out.println("Adding packet");
		final int idxFinal = idx;
		panel.remove(idx);
		panel.add(new PacketButton(new TestPacket() {
			@Override
			public void build() {
				super.writeInt8(idxFinal);
				super.writeInt8((byte) Math.max(0, (int)(Math.random()*5-2)));
			}
		}), idx);
		panel.revalidate();
		panel.repaint();
	}
	
	private class PacketButton extends JButton {
		private PacketButton(OutPacket packet) {
			super(packet.toString());
			
			super.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Network.send(packet);
				}
			});
		}
	}

}
