//package client.network.packet.in;
//
//import client.Client;
//import game.entities.Npc;
//import game.entities.Player;
//import shared.opcodes.ServerToClient;
//import todo.main;
//
//public class MapRegionPacket extends InPacket {
//
//	public MapRegionPacket() {
//		super(ServerToClient.UPDATE_MAP_REGION, new byte[4]);
//	}
//	
//    @Override
//    public void process() {
//		int regionX = readUInt16();
//		int regionY = readUInt16();
//		main.constructedViewport = false;
//		if (Player.xChunk == regionX && Player.yChunk == regionY && main.loadingStage == 2)
//			return;
//		Player.xChunk = regionX;
//		Player.yChunk = regionY;
//		Player.regionBaseTileX = (Player.xChunk - 6) * 8;
//		Player.regionBaseTileY = (Player.yChunk - 6) * 8;
//		main.inPlayerOwnedHouse = (Player.xChunk / 8 == 48 || Player.xChunk / 8 == 49) && Player.yChunk / 8 == 48;
//		if (Player.xChunk / 8 == 48 && Player.yChunk / 8 == 148)
//			main.inPlayerOwnedHouse = true;
//		main.loadingStage = 1;
//		main.lastRegionUpdateTime = System.currentTimeMillis();
////		gameScreenImageProducer.initDrawingArea();
////		drawLoadingMessages(1, "Loading - please wait.", null);
////		gameScreenImageProducer.drawGraphics(super.graphics, frameMode == ScreenMode.FIXED ? 4 : 0,
////				frameMode == ScreenMode.FIXED ? 4 : 0);
//		int regionCount = 0;
//		for (int xr = (Player.xChunk - 6) / 8; xr <= (Player.xChunk + 6) / 8; xr++)
//			for (int yr = (Player.yChunk - 6) / 8; yr <= (Player.yChunk + 6) / 8; yr++)
//				regionCount++;
//		main.inputStreamArray = new byte[regionCount][];
//		main.aByteArrayArray1247 = new byte[regionCount][];
//		main.anIntArray1234 = new int[regionCount];
//		main.mapfileRequests = new int[regionCount];
//		main.landscapeRequests = new int[regionCount];
//		regionCount = 0;
//		for (int xr = (Player.xChunk - 6) / 8; xr <= (Player.xChunk + 6) / 8; xr++) {
//			for (int yr = (Player.yChunk - 6) / 8; yr <= (Player.yChunk + 6) / 8; yr++) {
//				main.anIntArray1234[regionCount] = (xr << 8) + yr;
//				if (main.inPlayerOwnedHouse
//						&& (yr == 49 || yr == 149 || yr == 147 || xr == 50 || xr == 49 && yr == 47)) {
//					main.mapfileRequests[regionCount] = -1;
//					main.landscapeRequests[regionCount] = -1;
//				} else {
////					int mapfileRequest = main.mapfileRequests[regionCount] = resourceProvider.getAreaCode(0, yr, xr);
////					if (mapfileRequest != -1)
////						resourceProvider.provide(3, mapfileRequest);
////					int landscapeRequest = main.landscapeRequests[regionCount] = resourceProvider.getAreaCode(1, yr, xr);
////					if (landscapeRequest != -1)
////						resourceProvider.provide(3, landscapeRequest);
//				}
//				regionCount++;
//			}
//		}
//		
//		int xTileShift = Player.regionBaseTileX - main.previousRegionBaseTileX;
//		int yTileShift = Player.regionBaseTileY - main.previousRegionBaseTileY;
//		main.previousRegionBaseTileX = Player.regionBaseTileX;
//		main.previousRegionBaseTileY = Player.regionBaseTileY;
//		for (int i = 0; i < main.npcs.length; i++) {
//			Npc npc = main.npcs[i];
//			if (npc == null)
//				continue;
////			for (int j = 0; j < 10; j++) {
////				npc.pathX[j] -= xTileShift;
////				npc.pathY[j] -= yTileShift;
////			}
////			npc.x -= xTileShift * 128;
////			npc.y -= yTileShift * 128;
//		}
//		for (int i = 0; i < main.players.length; i++) {
//			Player player = main.players[i];
//			if (player == null)
//				continue;
////			for (int j = 0; j < 10; j++) {
////				player.pathX[j] -= xTileShift;
////				player.pathY[j] -= yTileShift;
////			}
////			player.x -= xTileShift * 128;
////			player.y -= yTileShift * 128;
//		}
////		validLocalMap = true;
//		byte byte1 = 0;
//		byte byte2 = 104;
//		byte byte3 = 1;
//		if (xTileShift < 0) {
//			byte1 = 103;
//			byte2 = -1;
//			byte3 = -1;
//		}
//		byte byte4 = 0;
//		byte byte5 = 104;
//		byte byte6 = 1;
//		if (yTileShift < 0) {
//			byte4 = 103;
//			byte5 = -1;
//			byte6 = -1;
//		}
////		for (int k33 = byte1; k33 != byte2; k33 += byte3) {
////			for (int l33 = byte4; l33 != byte5; l33 += byte6) {
////				int i34 = k33 + i17;
////				int j34 = l33 + j21;
////				for (int k34 = 0; k34 < 4; k34++)
////					if (i34 >= 0 && j34 >= 0 && i34 < 104 && j34 < 104)
////						groundItems[k34][k33][l33] = groundItems[k34][i34][j34];
////					else
////						groundItems[k34][k33][l33] = null;
////			}
////		}
////		for (SpawnedObject class30_sub1_1 = (SpawnedObject) spawns
////				.reverseGetFirst(); class30_sub1_1 != null; class30_sub1_1 = (SpawnedObject) spawns
////						.reverseGetNext()) {
////			class30_sub1_1.x -= i17;
////			class30_sub1_1.y -= j21;
////			if (class30_sub1_1.x < 0 || class30_sub1_1.y < 0 || class30_sub1_1.x >= 104
////					|| class30_sub1_1.y >= 104)
////				class30_sub1_1.unlink();
////		}
////		if (destinationX != 0) {
////			destinationX -= i17;
////			destinationY -= j21;
////		}
////		oriented = false;
//    }
//}
