//package client.network.packet.in;
//
//import client.Client;
//import game.entities.Npc;
//import game.entities.Player;
//import shared.io.Buffer;
//import shared.opcodes.ServerToClient;
//import todo.main;
//
//public class MobsUpdatePacket extends VarPacket {
//
//	protected MobsUpdatePacket() {
//		super(ServerToClient.MOBS_UPDATE, 2);
//	}
//	
//    @Override
//    public void process() {
//    	updateNPCs(this, this.array.length);
//    }
//
//	private void updateNPCs(Buffer stream, int i) {
//		main.anInt839 = 0;
//		main.mobsAwaitingUpdateCount = 0;
//		method139(stream);
//		updateNPCMovement(i, stream);
//		npcUpdateMask(stream);
//		for (int k = 0; k < main.anInt839; k++) {
//			int l = main.anIntArray840[k];
//			if (main.npcs[l].loopCycle != main.loopCycle) {
////				main.npcs[l].desc = null;
//				main.npcs[l] = null;
//			}
//		}
//	}
//
//	private void method139(Buffer stream) {
//		stream.initBitAccess();
//		int k = stream.readBits(8);
//		if (k < main.npcCount) {
//			for (int l = k; l < main.npcCount; l++) {
//				main.anIntArray840[main.anInt839++] = main.npcIndices[l];
//			}
//		}
//		main.npcCount = 0;
//		for (int i1 = 0; i1 < k; i1++) {
//			int j1 = main.npcIndices[i1];
//			Npc npc = main.npcs[j1];
//			int k1 = stream.readBits(1);
//			if (k1 == 0) {
//				main.npcIndices[main.npcCount++] = j1;
//				npc.loopCycle = main.loopCycle;
//			} else {
//				int l1 = stream.readBits(2);
//				if (l1 == 0) {
//					main.npcIndices[main.npcCount++] = j1;
//					npc.loopCycle = main.loopCycle;
//					main.mobsAwaitingUpdate[main.mobsAwaitingUpdateCount++] = j1;
//				} else if (l1 == 1) {
//					main.npcIndices[main.npcCount++] = j1;
//					npc.loopCycle = main.loopCycle;
//					int i2 = stream.readBits(3);
////					npc.moveInDir(false, i2);
//					int k2 = stream.readBits(1);
//					if (k2 == 1)
//						main.mobsAwaitingUpdate[main.mobsAwaitingUpdateCount++] = j1;
//				} else if (l1 == 2) {
//					main.npcIndices[main.npcCount++] = j1;
//					npc.loopCycle = main.loopCycle;
//					int j2 = stream.readBits(3);
////					npc.moveInDir(true, j2);
//					int l2 = stream.readBits(3);
////					npc.moveInDir(true, l2);
//					int i3 = stream.readBits(1);
//					if (i3 == 1)
//						main.mobsAwaitingUpdate[main.mobsAwaitingUpdateCount++] = j1;
//				} else if (l1 == 3)
//					main.anIntArray840[main.anInt839++] = j1;
//			}
//		}
//	}
//
//	private void updateNPCMovement(int i, Buffer stream) {
//		while (stream.bitPosition() + 21 < i * 8) {
//			int k = stream.readBits(14);
//			if (k == 16383)
//				break;
//			if (main.npcs[k] == null)
//				main.npcs[k] = new Npc();
//			Npc npc = main.npcs[k];
//			main.npcIndices[main.npcCount++] = k;
//			npc.loopCycle = main.loopCycle;
//			int l = stream.readBits(5);
//			if (l > 15)
//				l -= 32;
//			int i1 = stream.readBits(5);
//			if (i1 > 15)
//				i1 -= 32;
//			int j1 = stream.readBits(1);
////			npc.desc = NpcDefinition.lookup(
//					stream.readBits(12);
////					);
//			int k1 = stream.readBits(1);
//			if (k1 == 1)
//				main.mobsAwaitingUpdate[main.mobsAwaitingUpdateCount++] = k;
////			npc.boundDim = npc.desc.boundDim;
////			npc.degreesToTurn = npc.desc.degreesToTurn;
////			npc.walkAnimIndex = npc.desc.walkAnim;
////			npc.turn180AnimIndex = npc.desc.turn180AnimIndex;
////			npc.turn90CWAnimIndex = npc.desc.turn90CWAnimIndex;
////			npc.turn90CCWAnimIndex = npc.desc.turn90CCWAnimIndex;
////			npc.standAnimIndex = npc.desc.standAnim;
////			npc.setPos(localPlayer.pathX[0] + i1, localPlayer.pathY[0] + l, j1 == 1);
//		}
//		stream.finishBitAccess();
//	}
//
//	private void npcUpdateMask(Buffer stream) {
//		for (int j = 0; j < main.mobsAwaitingUpdateCount; j++) {
//			int k = main.mobsAwaitingUpdate[j];
//			Npc npc = main.npcs[k];
//			int l = stream.readUnsignedByte();
//			if ((l & 0x10) != 0) {
//				int i1 = stream.readUInt16();
//				if (i1 == 65535)
//					i1 = -1;
//				int i2 = stream.readUnsignedByte();
////				if (i1 == npc.emoteAnimation && i1 != -1) {
////					int l2 = Animation.animations[i1].anInt365;
////					if (l2 == 1) {
////						npc.displayedEmoteFrames = 0;
////						npc.emoteTimeRemaining = 0;
////						npc.animationDelay = i2;
////						npc.currentAnimationLoops = 0;
////					}
////					if (l2 == 2)
////						npc.currentAnimationLoops = 0;
////				} else if (i1 == -1 || npc.emoteAnimation == -1
////						|| Animation.animations[i1].anInt359 >= Animation.animations[npc.emoteAnimation].anInt359) {
////					npc.emoteAnimation = i1;
////					npc.displayedEmoteFrames = 0;
////					npc.emoteTimeRemaining = 0;
////					npc.animationDelay = i2;
////					npc.currentAnimationLoops = 0;
////					npc.anInt1542 = npc.smallXYIndex;
////				}
//			}
//			if ((l & 8) != 0) {
//				int j1 = stream.readUnsignedByte();
//				int j2 = stream.readUnsignedByte();
////				npc.updateHitData(j2, j1, loopCycle);
////				npc.loopCycleStatus = loopCycle + 300;
////				npc.currentHealth = 
//						stream.readUnsignedByte();
////				npc.maxHealth = 
//						stream.readUnsignedByte();
//			}
//			if ((l & 0x80) != 0) {
////				npc.gfxId = 
//						stream.readUInt16();
//				int k1 = stream.readUInt32();
////				npc.graphicHeight = k1 >> 16;
////				npc.graphicDelay = loopCycle + (k1 & 0xffff);
////				npc.currentAnimation = 0;
////				npc.anInt1522 = 0;
////				if (npc.graphicDelay > loopCycle)
////					npc.currentAnimation = -1;
////				if (npc.gfxId == 0xFFFF)
////					npc.gfxId = -1;
//			}
//			if ((l & 0x20) != 0) {
////				npc.interactingEntity = 
//						stream.readUInt16();
////				if (npc.interactingEntity == 0xFFFF)
////					npc.interactingEntity = -1;
//			}
//			if ((l & 1) != 0) {
////				npc.spokenText = 
//						stream.readString();
////				npc.textCycle = 100;
//			}
//			if ((l & 0x40) != 0) {
//				int l1 = stream.readUnsignedByte();
//				int k2 = stream.readUnsignedByte();
////				npc.updateHitData(k2, l1, loopCycle);
////				npc.loopCycleStatus = loopCycle + 300;
////				npc.currentHealth = 
//						stream.readUnsignedByte();
////				npc.maxHealth = 
//						stream.readUnsignedByte();
//			}
//			if ((l & 2) != 0) {
////				npc.desc = NpcDefinition.lookup(
//						stream.readUInt16();
////						);
////				npc.boundDim = npc.desc.boundDim;
////				npc.degreesToTurn = npc.desc.degreesToTurn;
////				npc.walkAnimIndex = npc.desc.walkAnim;
////				npc.turn180AnimIndex = npc.desc.turn180AnimIndex;
////				npc.turn90CWAnimIndex = npc.desc.turn90CWAnimIndex;
////				npc.turn90CCWAnimIndex = npc.desc.turn90CCWAnimIndex;
////				npc.standAnimIndex = npc.desc.standAnim;
//			}
//			if ((l & 4) != 0) {
////				npc.faceX = 
//						stream.readUInt16();
////				npc.faceY = 
//						stream.readUInt16();
//			}
//		}
//	}
//}
