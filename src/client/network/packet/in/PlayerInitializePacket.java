//package client.network.packet.in;
//
//import client.Client;
//import shared.opcodes.ServerToClient;
//
//public class PlayerInitializePacket extends InPacket {
//
//	public PlayerInitializePacket() {
//		super(ServerToClient.PLAYER_INITIALIZE, new byte[3]);
//	}
//	
//    @Override
//    public void process() {
//    	Client.player.setMember(super.readByte() == 0);
//    	Client.player.setId(super.readUInt16());
//    }
//    
//}