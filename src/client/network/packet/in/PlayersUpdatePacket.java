//package client.network.packet.in;
//
//import client.Client;
//import game.entities.Player;
//import shared.io.Buffer;
//import shared.opcodes.ServerToClient;
//import todo.main;
//
//public class PlayersUpdatePacket extends VarPacket {
//
//	protected PlayersUpdatePacket() {
//		super(ServerToClient.PLAYERS_UPDATE, 2);
//	}
//	
//    @Override
//    public void process() {
//    	main.anInt839 = 0;
//		main.mobsAwaitingUpdateCount = 0;
//		updatePlayerMovement(this);
//		method134(this);
//		updateOtherPlayerMovement(this, this.array.length);
//		refreshUpdateMasks(this);
//		for (int k = 0; k < main.anInt839; k++) {
//			int l = main.anIntArray840[k];
//			if (main.players[l].loopCycle != main.loopCycle)
//				main.players[l] = null;
//		}
//    }
//
//	private void updatePlayerMovement(Buffer stream) {
//		stream.initBitAccess();
//		int update = stream.readBits(1);
//		if (update == 0)
//			return;
//		int type = stream.readBits(2);
//		if (type == 0) {
//			main.mobsAwaitingUpdate[main.mobsAwaitingUpdateCount++] = main.internalLocalPlayerIndex;
//		} else if (type == 1) {
//			int direction = stream.readBits(3);
////			localPlayer.moveInDir(false, direction);
//			int updateRequired = stream.readBits(1);
//			if (updateRequired == 1)
//				main.mobsAwaitingUpdate[main.mobsAwaitingUpdateCount++] = main.internalLocalPlayerIndex;
//		} else if (type == 2) {
//			int firstDirection = stream.readBits(3);
////			localPlayer.moveInDir(true, firstDirection);
//			int secondDirection = stream.readBits(3);
////			localPlayer.moveInDir(true, secondDirection);
//			int updateRequired = stream.readBits(1);
//			if (updateRequired == 1)
//				main.mobsAwaitingUpdate[main.mobsAwaitingUpdateCount++] = main.internalLocalPlayerIndex;
//		} else if (type == 3) {
//	    	Client.player.getPosition().setZ(readBits(2));
//			int teleport = stream.readBits(1);
//			int updateRequired = stream.readBits(1);
//			if (updateRequired == 1)
//				main.mobsAwaitingUpdate[main.mobsAwaitingUpdateCount++] = main.internalLocalPlayerIndex;
//			int y = stream.readBits(7);
//			int x = stream.readBits(7);
////			localPlayer.setPos(x, y, teleport == 1);
//		}
//	}
//
//	private void method134(Buffer stream) {
//		int numberOfPlayers = stream.readBits(8);
//		if (numberOfPlayers < main.playerCount)
//			for (int k = numberOfPlayers; k < main.playerCount; k++)
//				main.anIntArray840[main.anInt839++] = main.playerIndices[k];
//		main.playerCount = 0;
//		for (int l = 0; l < numberOfPlayers; l++) {
//			int i1 = main.playerIndices[l];
//			Player player = main.players[i1];
//			int j1 = stream.readBits(1);
//			if (j1 == 0) {
//				main.playerIndices[main.playerCount++] = i1;
//				player.loopCycle = main.loopCycle;
//			} else {
//				int k1 = stream.readBits(2);
//				if (k1 == 0) {
//					main.playerIndices[main.playerCount++] = i1;
//					player.loopCycle = main.loopCycle;
//					main.mobsAwaitingUpdate[main.mobsAwaitingUpdateCount++] = i1;
//				} else if (k1 == 1) {
//					main.playerIndices[main.playerCount++] = i1;
//					player.loopCycle = main.loopCycle;
//					int l1 = stream.readBits(3);
////					player.moveInDir(false, l1);
//					int j2 = stream.readBits(1);
//					if (j2 == 1)
//						main.mobsAwaitingUpdate[main.mobsAwaitingUpdateCount++] = i1;
//				} else if (k1 == 2) {
//					main.playerIndices[main.playerCount++] = i1;
//					player.loopCycle = main.loopCycle;
//					int i2 = stream.readBits(3);
////					player.moveInDir(true, i2);
//					int k2 = stream.readBits(3);
////					player.moveInDir(true, k2);
//					int l2 = stream.readBits(1);
//					if (l2 == 1)
//						main.mobsAwaitingUpdate[main.mobsAwaitingUpdateCount++] = i1;
//				} else if (k1 == 3)
//					main.anIntArray840[main.anInt839++] = i1;
//			}
//		}
//	}
//
//	private void updateOtherPlayerMovement(Buffer stream, int packetSize) {
//		while (stream.bitPosition() + 10 < packetSize * 8) {
//			int j = stream.readBits(11);
//			if (j == 0x7FF)
//				break;
//			if (main.players[j] == null) {
//				main.players[j] = new Player();
////				if (main.playerSynchronizationBuffers[j] != null)
////					main.players[j].updatePlayer(main.playerSynchronizationBuffers[j]);
//			}
//			main.playerIndices[main.playerCount++] = j;
//			Player player = main.players[j];
//			player.loopCycle = main.loopCycle;
//			int k = stream.readBits(1);
//			if (k == 1)
//				main.mobsAwaitingUpdate[main.mobsAwaitingUpdateCount++] = j;
//			int l = stream.readBits(1);
//			int i1 = stream.readBits(5);
//			if (i1 > 15)
//				i1 -= 32;
//			int j1 = stream.readBits(5);
//			if (j1 > 15)
//				j1 -= 32;
////			player.setPos(localPlayer.pathX[0] + j1, localPlayer.pathY[0] + i1, l == 1);
//		}
//		stream.finishBitAccess();
//	}
//
//	private void refreshUpdateMasks(Buffer stream) {
//		for (int j = 0; j < main.mobsAwaitingUpdateCount; j++) {
//			int k = main.mobsAwaitingUpdate[j];
//			Player player = main.players[k];
//			int l = stream.readUnsignedByte();
//			if ((l & 0x40) != 0)
//				l += stream.readUnsignedByte() << 8;
//			appendPlayerUpdateMask(l, k, stream, player);
//		}
//	}
//
//	private void appendPlayerUpdateMask(int mask, int index, Buffer buffer, Player player) {
//		if ((mask & 0x400) != 0) {
////			player.initialX = 
//					buffer.readUnsignedByte();
////			player.initialY = 
//					buffer.readUnsignedByte();
////			player.destinationX = 
//					buffer.readUnsignedByte();
////			player.destinationY = 
//					buffer.readUnsignedByte();
////			player.startForceMovement = loopCycle + 
//					buffer.readUInt16();
////			player.endForceMovement = loopCycle +
//					buffer.readUInt16();
////			player.direction = 
//					buffer.readUnsignedByte();
////			player.resetPath();
//		}
//		if ((mask & 0x100) != 0) {
////			player.gfxId = 
//					buffer.readUInt16();
//			int info = buffer.readUInt32();
////			player.graphicHeight = info >> 16;
////			player.graphicDelay = loopCycle + (info & 0xffff);
////			player.currentAnimation = 0;
////			player.anInt1522 = 0;
////			if (player.graphicDelay > loopCycle)
////				player.currentAnimation = -1;
////			if (player.gfxId == 0xFFFF)
////				player.gfxId = -1;
//		}
//		if ((mask & 8) != 0) {
//			int animation = buffer.readUInt16();
//			if (animation == 0xFFFF)
//				animation = -1;
//			int delay = buffer.readUnsignedByte();
////			if (animation == player.emoteAnimation && animation != -1) {
////				int replayMode = Animation.animations[animation].anInt365;
////				if (replayMode == 1) {
////					player.displayedEmoteFrames = 0;
////					player.emoteTimeRemaining = 0;
////					player.animationDelay = delay;
////					player.currentAnimationLoops = 0;
////				}
////				if (replayMode == 2)
////					player.currentAnimationLoops = 0;
////			} else if (animation == -1 || player.emoteAnimation == -1
////					|| Animation.animations[animation].anInt359 >= Animation.animations[player.emoteAnimation].anInt359) {
////				player.emoteAnimation = animation;
////				player.displayedEmoteFrames = 0;
////				player.emoteTimeRemaining = 0;
////				player.animationDelay = delay;
////				player.currentAnimationLoops = 0;
////				player.anInt1542 = player.smallXYIndex;
////			}
//		}
//		if ((mask & 4) != 0) {
////			player.spokenText = 
//					buffer.readString();
////			if (player.spokenText.charAt(0) == '~') {
////				player.spokenText = player.spokenText.substring(1);
////				pushMessage(player.spokenText, 2, player.name);
////			} else if (player == localPlayer)
////				pushMessage(player.spokenText, 2, player.name);
////			player.textColour = 0;
////			player.textEffect = 0;
////			player.textCycle = 150;
//		}
//		if ((mask & 0x80) != 0) {
//			int textInfo = buffer.readUInt16();
//			int privilege = buffer.readUnsignedByte();
//			int offset = buffer.readUnsignedByte();
//			int off = buffer.position();
////			if (player.name != null && player.visible) {
////				long name = StringUtils.encodeBase37(player.name);
////				boolean ignored = false;
////				if (privilege <= 1) {
////					for (int count = 0; count < ignoreCount; count++) {
////						if (ignoreListAsLongs[count] != name)
////							continue;
////						ignored = true;
////						break;
////					}
////				}
////				if (!ignored && onTutorialIsland == 0) {
////					chatBuffer.reset();
////					buffer.read(chatBuffer.array(), offset, 0);
////					chatBuffer.reset();
////					String text = ChatMessageCodec.decode(offset, chatBuffer);
////					player.spokenText = text;
////					player.textColour = textInfo >> 8;
////					player.textEffect = textInfo & 0xff;
////					player.textCycle = 150;
////					if (privilege == 2 || privilege == 3)
////						pushMessage(text, 1, "@cr2@" + player.name);
////					else if (privilege == 1)
////						pushMessage(text, 1, "@cr1@" + player.name);
////					else
////						pushMessage(text, 2, player.name);
////				}
////			}
//			buffer.seek(off + offset);
//		}
//		if ((mask & 1) != 0) {
////			player.interactingEntity = 
//					buffer.readUInt16();
////			if (player.interactingEntity == 0xFFFF)
////				player.interactingEntity = -1;
//		}
//		if ((mask & 0x10) != 0) {
//			int length = buffer.readUnsignedByte();
//			byte data[] = new byte[length];
//			Buffer appearanceBuffer = new Buffer(data);
//			buffer.read(data, 0, length);
////			playerSynchronizationBuffers[index] = appearanceBuffer;
////			player.updatePlayer(appearanceBuffer);
//		}
//		if ((mask & 2) != 0) {
////			player.faceX = 
//					buffer.readUInt16();
////			player.faceY = 
//					buffer.readUInt16();
//		}
//		if ((mask & 0x20) != 0) {
//			int damage = buffer.readUnsignedByte();
//			int type = buffer.readUnsignedByte();
////			player.updateHitData(type, damage, loopCycle);
////			player.loopCycleStatus = loopCycle + 300;
////			player.currentHealth = 
//					buffer.readUnsignedByte();
////			player.maxHealth = 
//					buffer.readUnsignedByte();
//		}
//		if ((mask & 0x200) != 0) {
//			int damage = buffer.readUnsignedByte();
//			int type = buffer.readUnsignedByte();
////			player.updateHitData(type, damage, loopCycle);
////			player.loopCycleStatus = loopCycle + 300;
////			player.currentHealth = 
//					buffer.readUnsignedByte();
////			player.maxHealth = 
//					buffer.readUnsignedByte();
//		}
//	}
//}
