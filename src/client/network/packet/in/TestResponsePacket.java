package client.network.packet.in;

import javax.swing.JLabel;

import client.network.NetworkTest;

public class TestResponsePacket extends InPacket {

	public TestResponsePacket() {
		super(55, 2);
	}
	
    @Override
    public void process() {
    	System.out.println("PROCESSING");
    	
    	int location = super.readUInt8();
    	NetworkTest.panel.remove(location);
    	NetworkTest.panel.add(new JLabel(), location);
    	NetworkTest.panel.revalidate();
    	NetworkTest.panel.repaint();
    }
    
}