package client.network.packet.out;

public class EmptyPacket extends OutPacket {
    
    public EmptyPacket(int opcode) {
        super(opcode, 0);
    }

	@Override
	public void build() {
		
	}
}