package client.network.packet.out;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

import client.network.Packet;
 
public abstract class OutPacket extends Packet {
 
    protected OutPacket(int opcode, int length) {
        super(opcode, length);
        build();
    }
 
    protected OutPacket(int opcode) {
        super(opcode);
        build();
    }
     
//    protected OutPacket(int opcode, byte[] data) {
//        super(data);
//        this.opcode = (byte)opcode;
//    }
 
    public void send(OutputStream outputStream) {
        try {
            outputStream.write(opcode);
            outputStream.write(data, 0, position());
            System.out.println("Sending: "+Arrays.toString(data));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    abstract public void build();
}
