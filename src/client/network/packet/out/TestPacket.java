package client.network.packet.out;

public class TestPacket extends OutPacket {
	
	public TestPacket() {
		super(55, 2);
	}
	
	@Override
	public void build() {
	}

	@Override
	public String toString() {
		return "TICKS: "+super.data[1];
	}
}