package log;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {
	
	private static final SimpleDateFormat SDF = new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss.SSS] ");

	public static void log(Object o) {
		System.out.println(SDF.format(new Date()) + o.toString());
	}

}
