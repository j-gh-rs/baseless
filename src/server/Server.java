package server;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import server.network.Connection;
import server.network.Network;

public class Server implements Runnable {

	public static final int TICK = 600;
	public static final int MAX_PLAYERS = 100;
	public static final Player[] PLAYERS = new Player[MAX_PLAYERS];
	
	public Server() {
		Network.start();
		
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		executor.scheduleAtFixedRate(this, 0, 3, TimeUnit.SECONDS);
	}
	
	public static void main(String[] args) {
		new Server();
	}

	@Override
	public void run() {
		System.out.println("LOOOOP");
		for (Connection c : Network.connections)
			c.send();
	}

}
