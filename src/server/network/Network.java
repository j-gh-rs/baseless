package server.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

import server.Server;
import server.network.packet.out.OutPacket;

public class Network {
	
	public static ArrayList<Connection> connections = new ArrayList<Connection>();
	
	private static ServerSocket serverSocket;
	
	public static void start() {
		try {
			serverSocket = new ServerSocket(shared.Constants.PORT);
			new Thread() {
				public void run() {
					while (true) {
						try {
//							Connection c = new Connection(serverSocket.accept());
//							Player p = new Player(c);
//							Server.PLAYERS[0] = p;
						    connections.add(new Connection(serverSocket.accept()));
						    System.out.println("NEW CONNECTION");
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void send(int c, OutPacket packet) {
		Connection connection;
//		try {
			connection = connections.get(c);
//		} catch (IndexOutOfBoundsException e) {
//			e.printStackTrace();
//			return;
//		}
//		if (connection == null)
//			return;
		connection.send(packet);
	}
	
	public static void finish(int c) {
		Connection connection;
//		try {
			connection = connections.get(c);
//		} catch (IndexOutOfBoundsException e) {
//			e.printStackTrace();
//			return;
//		}
//		if (connection == null)
//			return;
		connection.finish();
	}
	
	public static void send(int c, int i) {
		Connection connection;
//		try {
			connection = connections.get(c);
//		} catch (IndexOutOfBoundsException e) {
//			return;
//		}
//		if (connection == null)
//			return;
		connection.send((byte)i);
	}

	public static int getInSize(int c) {
		Connection connection;
//		try {
			connection = connections.get(c);
//		} catch (IndexOutOfBoundsException e) {
//			return 0;
//		}
//		if (connection == null)
//			return 0;
		return connection.getInSize();
	}

	public static int getOutSize(int c) {
		Connection connection;
//		try {
			connection = connections.get(c);
//		} catch (IndexOutOfBoundsException e) {
//			return 0;
//		}
//		if (connection == null)
//			return 0;
		return connection.getOutSize();
	}
}