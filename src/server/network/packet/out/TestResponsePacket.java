package server.network.packet.out;

public class TestResponsePacket extends OutPacket {
	
	public TestResponsePacket() {
		super(55, 1);
	}
	
	@Override
	public void build() {
		
	}

	@Override
	public String toString() {
		return "TICKS: "+super.data[0];
	}
}