package shared;

public class Constants {
	public static final int PORT = 50000;
	public static final String HOST = "localhost";
	public static final int PACKET_HANDLE_DELAY = 100;

}
