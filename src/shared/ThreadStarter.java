package shared;

public class ThreadStarter {
	public static Thread startRunnable(Runnable runnable, int priority) {
		Thread thread = new Thread(runnable);
		thread.start();
		thread.setPriority(priority);
		return thread;
	}
}